﻿using System;
using NUnit.Framework;
using Standups.Controllers;
using Moq;
using Standups.Models.Abstract;
using Standups.Models.SUPModels;
using System.Web.Mvc;
using System.Linq;
using Microsoft.AspNet.Identity;   // had to install Microsoft.AspNet.Identity.Core from NuGet into Tests project. Select same version as in main project
using System.Security.Claims;
using System.Security.Principal;

namespace Tests.TestControllers
{
    [TestFixture]
    public class TestHome
    {
        private Mock<ISUPRepository> mock;

        [SetUp]
        public void SetupUserMock()
        {
            SUPGroup[] groups = new SUPGroup[] {
                    new SUPGroup { ID = 1, Name = "Group 1" },
                    new SUPGroup { ID = 2, Name = "Group 2" }
                    };

            mock = new Mock<ISUPRepository>();
            mock.Setup(m => m.SUPUsers)
                .Returns(
                new SUPUser[]
                {
                    new SUPUser {ID = 1, FirstName = "Bob", LastName = "Roberts", SUPGroupID = 1, SUPGroup = groups[0], ASPNetIdentityID = "id001"},
                    new SUPUser {ID = 2, FirstName = "Julie", LastName = "Juliet", SUPGroupID = 1, SUPGroup = groups[0], ASPNetIdentityID = "id002"},
                    new SUPUser {ID = 3, FirstName = "Rick", LastName = "Rickard", SUPGroupID = null, SUPGroup = null, ASPNetIdentityID = "id003"},
                    new SUPUser {ID = 4, FirstName = "Sue", LastName = "Suarez", SUPGroupID = 2, SUPGroup = groups[1], ASPNetIdentityID = "id004"},
                    new SUPUser {ID = 5, FirstName = "John", LastName = "Johanson", SUPGroupID = null, SUPGroup = null, ASPNetIdentityID = "id005"}
                });
        }

        [Test]
        public void UserNeedsGroup()
        {
            // Arrange
            HomeController controller = new HomeController(mock.Object);
            // Act
            SUPUser u = mock.Object.SUPUsers.ElementAt(2);
            // Assert
            Assert.That(controller.SUPUserNeedsGroupSet(u), Is.True);
        }

        [Test]
        public void UserDoesNotNeedGroup()
        {
            // Arrange
            HomeController controller = new HomeController(mock.Object);
            // Act
            SUPUser u = mock.Object.SUPUsers.ElementAt(0);
            // Assert
            Assert.That(controller.SUPUserNeedsGroupSet(u), Is.False);
        }

        public ControllerContext GetMockControllerContext(SUPUser u)
        {
            // mock parts of Identity:
            // Using parts of http://stackoverflow.com/questions/22762338/how-do-i-mock-user-identity-getuserid    and
            // http://stackoverflow.com/questions/758066/how-to-mock-controller-user-using-moq

            // mock an Identity with a known id
            var identity = new GenericIdentity("TestUsername");
            identity.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", u.ASPNetIdentityID));
            //   This line above is fragile in that is assumes ASP.NET Identity looks for exactly this string for the claim key string.
            //   It will fail if not an exact match.  If it fails then the lookup fails and GetUserId() returns null, meaning no User
            //   is found.  Look at the source code for: IdentityExtensions.GetUserId()

            // mock an IPrincipal that uses the mock identity
            Mock<IPrincipal> mockIPrincipal = new Mock<IPrincipal>();
            mockIPrincipal.Setup(p => p.Identity)
                          .Returns(identity);

            // mock a controller context for the controller to use
            Mock<ControllerContext> mockControllerContext = new Mock<ControllerContext>();
            mockControllerContext.Setup(p => p.HttpContext.User)
                                 .Returns(mockIPrincipal.Object);

            return mockControllerContext.Object;
        }

        /*
         * Need to mock the User Manager for these to run
         * */

        [Test]
        public void AuthenticatedUserWhoNeedsGroup_ShouldSetViewData()
        {
            HomeController controller = new HomeController(mock.Object);

            // user for this test, who needs to have a group set
            SUPUser u = mock.Object.SUPUsers.ElementAt(2);

            // Use the context, which will return the given identity id
            controller.ControllerContext = GetMockControllerContext(u);

            // Generate enough of the view to hold the ViewBag data we set there
            ViewResult index = (ViewResult) controller.Index(null);

            // See if it got set
            Assert.That(index.ViewData["NeedsGroup"], Is.True);
        }

        // NOTE: need to refactor this duplicated code into a common method
        [Test]
        public void AuthenticatedUserWhoNeedsNOGroup_ShouldNOTSetViewData()
        {
            HomeController controller = new HomeController(mock.Object);
            // user for this test who already has a group set
            SUPUser u = mock.Object.SUPUsers.ElementAt(1);

            // Use the context, which will return the given identity id
            controller.ControllerContext = GetMockControllerContext(u);

            // Generate enough of the view to hold the ViewBag data we set there
            ViewResult index = (ViewResult)controller.Index(null);

            // See if it got set
            Assert.That(index.ViewData["NeedsGroup"], Is.False);
        }
    }
}
