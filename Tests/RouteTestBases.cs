﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using NUnit.Framework;
using MvcRouteTester;

namespace Tests
{
    // https://github.com/anthonysteele/mvcroutetester/issues/20

    // Class to apply mvcroutetester to areas
    public abstract class AreaRouteFactsBase<T>
        where T : AreaRegistration, new()
    {
        public RouteCollection Routes { get; private set; }

        public AreaRouteFactsBase()
        {
            // Arrange
            Routes = new RouteCollection();
            T area = new T();
            AreaRegistrationContext context = new AreaRegistrationContext(area.AreaName, Routes);
            area.RegisterArea(context);
        }
    }

    // Class to make writing test classes easier.  Just extend this and use Routes
    public class TestRoutesBase
    {
        public RouteCollection Routes;

        //public TestRoutesBase()
        [SetUp]
        public void SetUp()
        {
            Routes = RouteTable.Routes;
            Standups.RouteConfig.RegisterRoutes(Routes);

            RouteAssert.UseAssertEngine(new NunitAssertEngine());
        }

        //public void Dispose()
        [TearDown]
        public void TearDown()
        {
            RouteTable.Routes.Clear();
        }
    }
}
