﻿using NUnit.Framework;
using System;
using MvcRouteTester.Assertions;
using MvcRouteTester;
using System.Web.Routing;
using Standups;
using Standups.Controllers;
using System.Web.Mvc;

namespace Tests
{
    [TestFixture]
    public class TestPublicRoutes : TestRoutesBase
    {
        [SetUp]
        public void SetupRouteTests()
        {

        }

        [Test]
        public void DefaultOverall_ShouldMapToHome_Index()
        {
            Routes.ShouldMap("/").To<HomeController>(x => x.Index(null));
        }

        [Test]
        public void DefaultHome_ShouldMapToHome_Index()
        {
            Routes.ShouldMap("/Home").To<HomeController>(x => x.Index(null));
        }

        [Test]
        public void DefaultHomeIndex_ShouldMapToHome_Index()
        {
            Routes.ShouldMap("/Home/Index").To<HomeController>(x => x.Index(null));
        }

        [Test]
        public void Contact_ShouldMapToHome_Contact()
        {
            Routes.ShouldMap("/Contact").To<HomeController>(x => x.Contact());
        }

        [Test]
        public void ContactPlusAnything_ShouldMapToHome_Contact()
        {
            Routes.ShouldMap("/Contact/something/else").To<HomeController>(x => x.Contact());
            Routes.ShouldMap("/contact/aalk8_literally/any/length/url?q=45").To<HomeController>(x => x.Contact());
        }

        [Test]
        public void About_ShouldMapToHome_About()
        {
            Routes.ShouldMap("/About").To<HomeController>(x => x.About());
        }

        [Test]
        public void AboutPlusAnything_ShouldMapToHome_About()
        {
            Routes.ShouldMap("/About/something/else").To<HomeController>(x => x.About());
            Routes.ShouldMap("/about/aalk8_literally/any/length/url?q=45").To<HomeController>(x => x.About());
        }

        [Test]
        public void TestURLWithQueryStrings()
        {
            //TestRouteMatch("~/Home/Index?id=5&hello=five", "Home", "Index", new { id = "5", hello = "five" });
            //var route = "/Home/Index".WithMethod(HttpVerbs.Get);
            //route.Values.Add("id", "5");
            //route.Values.Add("hello", "five");
            //route.ShouldMap("/Home/Index?id=5&hello=five").To<HomeController>(x => x.Index(5, "five"));
            //Routes.ShouldMap("/Home/Index?id=5&hello=five").To<HomeController>(x => x.Index(5,"five"));
        }

    }
}

