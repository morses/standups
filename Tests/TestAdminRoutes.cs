﻿using NUnit.Framework;
using System;
using MvcRouteTester.Assertions;
using MvcRouteTester;
using System.Web.Routing;
using Standups;
using Standups.Controllers;
using System.Web.Mvc;
using Standups.Areas.Admin.Controllers;

namespace Tests
{
    [TestFixture]
    public class TestAdminRoutes : AreaRouteFactsBase<Standups.Areas.Admin.AdminAreaRegistration>
    {

        [SetUp]
        public void SetupRouteTests()
        {
            RouteAssert.UseAssertEngine(new NunitAssertEngine());
        }

        [Test]
        public void DefaultRouteInAdmin_ShouldMapToIndexInAdminArea()
        {
            Routes.ShouldMap("/Admin").To<AdminController>(x => x.Index());
        }

        [Test]
        public void AdminAdmin_ShouldMapToAdminAreaIndex()
        {
            Routes.ShouldMap("/Admin/Admin").To<AdminController>(x => x.Index());
        }

        [Test]
        public void AdminAdvisors_CRUD_GET()
        {
            Routes.ShouldMap("/Admin/Advisors").To<AdvisorsController>(x => x.Index());
            Routes.ShouldMap("/Admin/Advisors/Create").To<AdvisorsController>(x => x.Create());
            Routes.ShouldMap("/Admin/Advisors/Details/5").To<AdvisorsController>(x => x.Details(5));
            Routes.ShouldMap("/Admin/Advisors/Edit/5").To<AdvisorsController>(x => x.Edit(5));
            Routes.ShouldMap("/Admin/Advisors/Delete/5").To<AdvisorsController>(x => x.Delete(5));
        }

        [Test]
        public void AdminGroups_CRUD_GET()
        {
            Routes.ShouldMap("/Admin/Groups").To<GroupsController>(x => x.Index());
            Routes.ShouldMap("/Admin/Groups/Create").To<GroupsController>(x => x.Create());
            Routes.ShouldMap("/Admin/Groups/Details/5").To<GroupsController>(x => x.Details(5));
            Routes.ShouldMap("/Admin/Groups/Edit/5").To<GroupsController>(x => x.Edit(5));
            Routes.ShouldMap("/Admin/Groups/Delete/5").To<GroupsController>(x => x.Delete(5));
        }

        [Test]
        public void AdminUsers_CRUD_GET()
        {
            Routes.ShouldMap("/Admin/Users").To<UsersController>(x => x.Index());
            Routes.ShouldMap("/Admin/Users/Create").To<UsersController>(x => x.Create());
            Routes.ShouldMap("/Admin/Users/Details/5").To<UsersController>(x => x.Details(5));
            Routes.ShouldMap("/Admin/Users/Edit/5").To<UsersController>(x => x.Edit(5));
            Routes.ShouldMap("/Admin/Users/Delete/5").To<UsersController>(x => x.Delete(5));
        }

        [Test]
        public void AdminReports_CRUD_GET()
        {
            Routes.ShouldMap("/Admin/Reports").To<ReportsController>(x => x.Index());
            Routes.ShouldMap("/Admin/Reports/Create").To<ReportsController>(x => x.Create());
            Routes.ShouldMap("/Admin/Reports/Details/5").To<ReportsController>(x => x.Details(5));
            Routes.ShouldMap("/Admin/Reports/Edit/5").To<ReportsController>(x => x.Edit(5));
            Routes.ShouldMap("/Admin/Reports/Delete/5").To<ReportsController>(x => x.Delete(5));
        }
    }
}
