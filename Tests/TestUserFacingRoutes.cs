﻿using System;
using NUnit.Framework;
using MvcRouteTester;
using Standups.Controllers;
using System.Net.Http;

namespace Tests
{

    [TestFixture]
    public class TestUserFacingRoutes : TestRoutesBase
    {
        [SetUp]
        public void SetupRouteTests()
        {

        }

        [Test]
        public void MyAccount_ShouldMapTo_MyAccount_Index()
        {
            Routes.ShouldMap("/MyAccount").To<MyAccountController>(x => x.Index());
        }

        [Test]
        public void MyAccountIndex_ShouldMapTo_MyAccount_Index()
        {
            Routes.ShouldMap("/MyAccount/Index").To<MyAccountController>(x => x.Index());
        }

        [Test]
        public void MyAccountSelectGroup_ShouldMapTo_MyAccount_SelectGroup_GET()
        {
            Routes.ShouldMap("/MyAccount/SelectGroup").To<MyAccountController>(x => x.SelectGroup());
        }

        [Test]
        public void MyAccountSelectGroup_ShouldMapTo_MyAccount_SelectGroup_POST()
        {
            Routes.ShouldMap("/MyAccount/SelectGroup/2").WithFormUrlBody("SUPGroupID=2").To<MyAccountController>(HttpMethod.Post, x => x.SelectGroup(2));
        }
    }
}
