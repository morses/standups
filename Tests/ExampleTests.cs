﻿using System;
using Moq;
using NUnit.Framework;
using Standups.Models.Example;

namespace Tests.Examples
{
    [TestFixture]
    public class ExampleTests
    {
        [Test]
        public void Test_A_Property()
        {
            // Arrange
            string expected = "George";
            Mock<IAccountRepository> mock = new Mock<IAccountRepository>();
            mock.Setup(m => m.Name)
                .Returns(expected);

            // Act 
            IAccountRepository ar = mock.Object;
            string name = ar.Name;
            // in reality, do something useful with this IAccountRepository instance

            // Assert
            Assert.That(name, Is.EqualTo(expected));
        }

        [Test]
        public void Test_Parameterless_Method()
        {
            Mock<IAccountRepository> mock = new Mock<IAccountRepository>();
            mock.Setup(m => m.IsValid())
                .Returns(true);

            IAccountRepository ar = mock.Object;

            Assert.That(ar.IsValid(), Is.True);
        }

        [Test]
        public void Test_SimpleParameter_Method()
        {
            Mock<IAccountRepository> mock = new Mock<IAccountRepository>();
            mock.Setup(m => m.GetBaseAccountNumber(5))
                .Returns(100);

            IAccountRepository ar = mock.Object;

            Assert.That(ar.GetBaseAccountNumber(5), Is.EqualTo(100));
        }

        [Test]
        public void Test_SimpleParameter_Method_WithAny()
        {
            Mock<IAccountRepository> mock = new Mock<IAccountRepository>();
            mock.Setup(m => m.GetBaseAccountNumber(It.IsAny<int>()))
                .Returns(100);
          
            IAccountRepository ar = mock.Object;

            Assert.That(ar.GetBaseAccountNumber(88), Is.EqualTo(100));
        }

        [Test]
        public void Test_Method_Taking_An_Object()
        {
            ExampleAccount ea = new ExampleAccount() { AccountNumber = 12345 };
            Mock<IAccountRepository> mock = new Mock<IAccountRepository>();
            mock.Setup(m => m.Update(It.IsAny<ExampleAccount>()))
                .Returns(true);

            IAccountRepository ar = mock.Object;

            Assert.That(ar.Update(ea), Is.True);
        }

        [Test]
        public void Test_Method_Returning_An_Object()
        {
            Mock<IAccountRepository> mock = new Mock<IAccountRepository>();
            mock.Setup(m => m.GetNewAccount())
                .Returns(
                    new ExampleAccount { AccountNumber = 42, IsOpen = true}
                 );

            IAccountRepository ar = mock.Object;

            Assert.That(ar.GetNewAccount().IsOpen, Is.True);
        }
    }
}
