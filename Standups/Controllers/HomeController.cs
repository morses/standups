﻿using Microsoft.AspNet.Identity;
using Standups.Models;
using Standups.Models.Abstract;
using Standups.Models.SUPModels;
using Standups.Models.SUPViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Standups.Controllers
{
    public class HomeController : Abstract.BaseControllerWithIdentity
    {
        public HomeController(ISUPRepository repo, ISUPCommentsRepository repo2) : base(repo,repo2)
        {

        }

        [NonAction]
        public bool SUPUserNeedsGroupSet(SUPUser user)
        {
            if(user == null || (user.SUPGroup == null && user.SUPGroupID == null))
            {
                return true;
            }
            return false;
        }

        // To see if route testing can do this one.
        public ActionResult IndexOther(int id, string hello)
        {
            return null;
        }

        public ActionResult Index(AccountMessageId? message)
        {
            // Need to generalize the message type, shouldn't only be "Account"
            ViewBag.StatusMessage =
                message == AccountMessageId.EmailSentSuccess ? "An email has been sent; please check your inbox."
                : message == AccountMessageId.EmailConfirmationNeeded ? ""
                : "";

            ViewBag.ShowTemporaryMessage = message == AccountMessageId.EmailSentSuccess;

            // Registered users (that aren't admin) must select which group they are in before they can
            // create a meeting report
            ViewBag.NeedsGroup = false;
            ViewBag.NeedsEmailConfirmed = false;
            if (IsAuthenticated() && !IsAdmin())
            {
                SUPUser user = GetCurrentSUPUser();
                //Debug.WriteLine("Current user: " + user);
                if (SUPUserNeedsGroupSet(user))
                {
                    ViewBag.NeedsGroup = true;
                }
                // Also see if they need to confirm their email address
                if (!UserManager.IsEmailConfirmed(GetCurrentIdentityID()))
                {
                    ViewBag.NeedsEmailConfirmed = true;
                }
            }
            // Fill the view model for this page
            HomePageViewModel vm = new HomePageViewModel
            {
                // Current open questions for the students to comment on
                Questions = commentsRepository.Questions.Include(q => q.SUPComments).Where(q => q.Active > 0).ToList()
            };

            return View(vm);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

    }
}