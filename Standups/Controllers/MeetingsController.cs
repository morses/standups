﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Standups.Models.SUPModels;
using Microsoft.AspNet.Identity;
using Standups.Models.SUPViewModels;

namespace Standups.Controllers
{
    // Need to refactor this class to use the repository

    [Authorize]
    public class MeetingsController : Controller
    {
        private SUPContext db = new SUPContext();

        // GET: Meetings
        public ActionResult Index()
        {
            string id = User.Identity.GetUserId();
            SUPUser user = db.SUPUsers.FirstOrDefault(u => u.ASPNetIdentityID == id);
            var sUPMeetings = db.SUPMeetings.Where(m => m.SUPUser.ID == user.ID).OrderByDescending(m => m.SubmissionDate).ToList();

            MeetingsViewModel vm = new MeetingsViewModel
            {
                CurrentUser = user,
                Meetings = sUPMeetings
            };

            return View(vm);
        }

        // GET: Meetings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                // this version works fine but depends on the exact path of the error view compared to this action method's view
                return View("../Errors/Http404");
                // this one sends a redirect (302), which may not be what is desired, i.e. if it's not found then it should
                // return a 404, not a 302 which directs to a "404" page that may just return a 200!
                //return RedirectToAction("Http404", "Errors");
                // this one doesn't return a view, just sets the response code in the reply
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            // Get all meetings for this user
            string iid = User.Identity.GetUserId();
            SUPUser user = db.SUPUsers.FirstOrDefault(u => u.ASPNetIdentityID == iid);
            var sUPMeetings = db.SUPMeetings.Where(m => m.SUPUser.ID == user.ID);
            // then look for the one with the given index
            SUPMeeting sUPMeeting = sUPMeetings.Where(m => m.ID == id).FirstOrDefault();
            if (sUPMeeting == null)
            {
                return View("../Errors/Http404");
                //return HttpNotFound();
            }
            return View(sUPMeeting);
        }

        // GET: Meetings/Create
        public ActionResult Create()
        {
            string id = User.Identity.GetUserId();
            SUPUser user = db.SUPUsers.FirstOrDefault(u => u.ASPNetIdentityID == id);
            SUPMeeting mtg = new SUPMeeting { SUPUser = user };
            return View(mtg);
        }

        // POST: Meetings/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Completed,Planning,Obstacles")] SUPMeeting sUPMeeting)
        {
            string id = User.Identity.GetUserId();
            SUPUser user = db.SUPUsers.FirstOrDefault(u => u.ASPNetIdentityID == id);
            sUPMeeting.SUPUser = user;
            sUPMeeting.SUPUserID = user.ID;

            if (ModelState.IsValid)
            {
                sUPMeeting.SubmissionDate = DateTime.UtcNow;
                db.SUPMeetings.Add(sUPMeeting);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sUPMeeting);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
