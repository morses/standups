﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Standups.Models.SUPModels;
using Standups.Models.Abstract;

namespace Standups.Controllers
{
    [Authorize]
    public class UserCommentsController : Abstract.BaseControllerWithIdentity
    {
        public UserCommentsController(ISUPRepository repo, ISUPCommentsRepository repo2) : base(repo, repo2)
        {
            
        }

        public ActionResult Rate(int? id)
        {
 /*           int idUse;
            if (id == null)
            {
                // Select the "first" one
                // newest
                idUse = commentsRepository.Questions
                        .Where(q => q.Active == 1)
                        .OrderByDescending(q => q.SubmissionDate)
                        .First()
                        .ID;
                // one with the most ratings?
                // with the best ratings?
            }
            else
            {
                idUse = id.Value;
            }
            ViewBag.QuestionID = idUse;
*/
            var questionsAvailable = commentsRepository.Questions.Where(q => q.Active == 1).ToList();

            return View(questionsAvailable);
        }

        public JsonResult Comments(int? id)
        {
            //System.Diagnostics.Debug.WriteLine($"Comments {id}");
            if(id == null)
            {
                return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            }
            SUPQuestion q = commentsRepository.GetQuestionById(id.Value);
            if(q != null && q.Active == 1)
            {
                SUPUser user = GetCurrentSUPUser();

                var data = q.SUPComments.Where(c => c.AdvisorRating > 0).Select(c => new {
                    ID = c.ID,
                    TimeStamp = c.SubmissionDate,
                    UserRating = c.SUPCommentRatings.FirstOrDefault(cr => cr.SUPRaterUserID == (user?.ID ?? -1))?.RatingValue ?? 0,
                    Likes = c.SUPCommentRatings.Where(cr => cr.RatingValue > 0).Count(),
                    Dislikes = c.SUPCommentRatings.Where(cr => cr.RatingValue < 0).Count(),
                    Comment = c.Comment});
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new List<object>(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult MakeRating()
        {
            string statusValue = "NOT OK";
            string cID = Request.Form["CommentID"];
            string act = Request.Form["Action"];
            int rating = 0;
            if(act == "UP")
            {
                rating = 1;
            }
            else if(act == "DOWN")
            {
                rating = -1;
            }

            int commentID;
            if(int.TryParse(cID, out commentID))
            {
                SUPUser user = GetCurrentSUPUser();
                if (user != null && commentsRepository.MakeCommentRating(user.ID, commentID, rating))
                {
                    statusValue = "OK";
                }
            }

            var data = new { status = statusValue };
            return Json(data);
        }

        // GET: UserComments/Create
        public ActionResult Create(int? questionID)
        {
            if (questionID == null)
            {
                return View("../Errors/Http404");
            }
            SUPQuestion q = commentsRepository.GetQuestionById(questionID.Value);
            if(q == null || q.Active < 1)
            {
                // Show them a page that says this question isn't open any more
                return RedirectToAction("QuestionUnavailable");
            }
            SUPComment sUPComment = new SUPComment
            {
                SUPQuestionID = questionID.Value,
                SUPQuestion = q
            };
            return View(sUPComment);
        }

        // POST: UserComments/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SUPQuestionID,Comment")] SUPComment sUPComment)
        {
            if (ModelState.IsValid)
            {
                sUPComment.SubmissionDate = DateTime.UtcNow;
                // new comments have an advisor rating of 0 to indicate they have not yet been reviewed for suitability to post
                sUPComment.AdvisorRating = 0;
                commentsRepository.InsertOrUpdateComment(sUPComment);
                commentsRepository.Save();
                return RedirectToAction("Thanks", new { questionID = sUPComment.SUPQuestionID });
            }

            return View(sUPComment);
        }

        public ActionResult QuestionUnavailable()
        {
            return View();
        }

        public ActionResult Thanks(int? questionID)
        {
            if (questionID == null)
            {
                return View("../Errors/Http404");
            }
            SUPQuestion q = commentsRepository.GetQuestionById(questionID.Value);
            if (q == null || q.Active < 1)
            {
                // Show them a page that says this question isn't open any more
                return RedirectToAction("QuestionUnavailable");
            }
            return View(q);
        }
    }
}
