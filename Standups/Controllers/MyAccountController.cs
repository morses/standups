﻿using Microsoft.AspNet.Identity;
using Standups.Models.SUPModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Standups.Controllers
{
    [Authorize]
    public class MyAccountController : Controller
    {
        private SUPContext db = new SUPContext();

        // GET: MyAccount
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated && !User.IsInRole("Admin"))
            {
                string id = User.Identity.GetUserId();
                SUPUser user = db.SUPUsers.FirstOrDefault(u => u.ASPNetIdentityID == id);
                return View(user);
            }
            else if(User.IsInRole("Admin"))
            {
                return View(new SUPUser());
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // Allow the current user to select which group they are in
        // GET: MyAccount/SelectGroup
        public ActionResult SelectGroup()
        {
            if (User.Identity.IsAuthenticated && !User.IsInRole("Admin"))
            {
                string id = User.Identity.GetUserId();
                SUPUser user = db.SUPUsers.FirstOrDefault(u => u.ASPNetIdentityID == id);
                ViewBag.SUPGroupID = new SelectList(db.SUPGroups, "ID", "Name", user.SUPGroupID);
                return View();
            }
            else if (User.IsInRole("Admin"))
            {
                return View();
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: MyAccount/SelectGroup
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SelectGroup(int SUPGroupID)
        {
            int gid = SUPGroupID;
            // Get the currently authenticated user
            string id = User.Identity.GetUserId();
            SUPUser user = db.SUPUsers.FirstOrDefault(u => u.ASPNetIdentityID == id);
            if (ModelState.IsValid && db.SUPGroups.Any(g => g.ID == gid))
            {
                user.SUPGroupID = gid;
                // Only update the group id, not everything else
                db.SUPUsers.Attach(user);
                db.Entry(user).Property(u => u.SUPGroupID).IsModified = true;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            ViewBag.SUPGroupID = new SelectList(db.SUPGroups, "ID", "Name", user.SUPGroupID);
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}