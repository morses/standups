﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Standups.Models.Abstract;
using Standups.Models.SUPModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Standups.Controllers.Abstract
{
    public abstract class BaseControllerWithIdentity : Controller
    {
        protected ISUPRepository repository;
        protected ISUPCommentsRepository commentsRepository;

        private ApplicationUserManager _userManager;

        public BaseControllerWithIdentity(ISUPRepository repo, ISUPCommentsRepository repo2)
        {
            this.repository = repo;
            this.commentsRepository = repo2;
        }

        /// <summary>
        /// Get the SUPUser object corresponding to the currently logged in user
        /// </summary>
        /// <returns>The currently logged in user as a SUPUser or null if not logged 
        /// in or no user exists in the database that has that Identity ID</returns>
        protected SUPUser GetCurrentSUPUser()
        {
            string id = User.Identity.GetUserId();
            SUPUser user = repository.SUPUsers.FirstOrDefault(u => u.ASPNetIdentityID == id);

            return user;
        }

        protected string GetCurrentIdentityID()
        {
            return User.Identity.GetUserId();
        }

        /// <summary>
        /// Helper method to access Identity to see if the current request comes from
        /// a logged in user.
        /// </summary>
        /// <returns>true if the user is authenticated, false otherwise</returns>
        protected bool IsAuthenticated()
        {
            return User.Identity.IsAuthenticated;
        }

        protected bool IsAdmin()
        {
            return User.IsInRole("Admin");
            //return false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                repository.Dispose();
                commentsRepository.Dispose();
            }
            base.Dispose(disposing);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}
