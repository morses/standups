﻿using Standups.Models.SUPModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Standups.Models.Abstract
{
    public interface ISUPCommentsRepository : IDisposable
    {
        // Supporting CRUD in a repository pattern isn't a good use of it.
        // What we really want here is customized data access.  CRUD exposes the 
        // lowest level access to the db entities, which we really want to protect.
        // Do it here anyway, for practice

        // IEnumerable or IQueryable? See: https://stackoverflow.com/questions/2876616/returning-ienumerablet-vs-iqueryablet
        IQueryable<SUPQuestion> Questions { get; }
        IQueryable<SUPComment> Comments { get; }
        IQueryable<SUPComment> GetCommentsFor(int questionID);
       
        // CRUD support for Questions
        SUPQuestion GetQuestionById(int id);
        SUPQuestion InsertOrUpdateQuestion(SUPQuestion question);
        void DeleteQuestion(int id);

        // CRUD support for Comments
        SUPComment GetCommentById(int id);
        SUPComment InsertOrUpdateComment(SUPComment comment);
        void DeleteComment(int id);

        // Ratings functionality
        bool MakeCommentRating(int userID, int commentID, int rating);

        // Interesting and useful methods


        void Save();
    }
}
