﻿using Standups.Models.SUPModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Standups.Models.Abstract
{
    public interface ISUPRepository : IDisposable
    {
        IEnumerable<SUPUser> SUPUsers { get; }
        IEnumerable<SUPAdvisor> SUPAdvisors { get; }
        IEnumerable<SUPGroup> SUPGroups { get; }
        IEnumerable<SUPMeeting> SUPMeetings { get; }

    }
}
