namespace Standups.Models.SUPModels
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SUPContext : DbContext
    {
        //public SUPContext() : base("name=SUPContext")
        public SUPContext() : base("name=AzureContext")
            //base("name=LocalTestingContext")
        {
        }

        public virtual DbSet<SUPUser> SUPUsers { get; set; }
        public virtual DbSet<SUPGroup> SUPGroups { get; set; }
        public virtual DbSet<SUPAdvisor> SUPAdvisors { get; set; }
        public virtual DbSet<SUPMeeting> SUPMeetings { get; set; }

        public virtual DbSet<SUPQuestion> SUPQuestions { get; set; }
        public virtual DbSet<SUPComment> SUPComments { get; set; }
        public virtual DbSet<SUPCommentRating> SUPCommentRatings { get; set; }
        public virtual DbSet<SUPAcademicYear> SUPAcademicYears { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SUPAcademicYear>()
                .Property(e => e.Year)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SUPAcademicYear>()
                .HasMany(e => e.SUPGroups)
                .WithRequired(e => e.SUPAcademicYear)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SUPComment>()
                .HasMany(e => e.SUPCommentRatings)
                .WithRequired(e => e.SUPComment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SUPQuestion>()
                .HasMany(e => e.SUPComments)
                .WithRequired(e => e.SUPQuestion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SUPUser>()
                .HasMany(e => e.SUPCommentRatings)
                .WithRequired(e => e.SUPUser)
                .HasForeignKey(e => e.SUPRaterUserID)
                .WillCascadeOnDelete(false);

//            modelBuilder.Entity<SUPUser>()
//                .HasMany(e => e.SUPComments)
//                .WithRequired(e => e.SUPUser)
//                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SUPUser>()
                .HasMany(e => e.SUPMeetings)
                .WithRequired(e => e.SUPUser)
                .WillCascadeOnDelete(false);
        }
    }
}
