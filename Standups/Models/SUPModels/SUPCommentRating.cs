namespace Standups.Models.SUPModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SUPCommentRating
    {
        public int ID { get; set; }

        public int SUPCommentID { get; set; }

        public int SUPRaterUserID { get; set; }

        public DateTime RatingDate { get; set; }

        public int RatingValue { get; set; }

        public virtual SUPComment SUPComment { get; set; }

        public virtual SUPUser SUPUser { get; set; }
    }
}
