﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Standups.Models.Example
{
    public class ExampleAccount
    {
        public int AccountNumber { get; set; }
        public bool IsOpen { get; set; } = false;
    }

    // An example repository for mocking example
    public interface IAccountRepository
    {
        // Simple property
        string Name { get; set; }

        // Parameterless method
        bool IsValid();

        // Primitive type parameter 
        int GetBaseAccountNumber(int num);

        // Object type parameter
        bool Update(ExampleAccount ea);

        // Object type returned
        ExampleAccount GetNewAccount();
    }
}
