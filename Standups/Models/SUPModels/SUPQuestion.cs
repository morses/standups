namespace Standups.Models.SUPModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SUPQuestion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SUPQuestion()
        {
            SUPComments = new HashSet<SUPComment>();
        }

        public int ID { get; set; }

        public DateTime SubmissionDate { get; set; }

        [Required]
        [StringLength(1000)]
        public string Question { get; set; }

        public int Active { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SUPComment> SUPComments { get; set; }
    }
}
