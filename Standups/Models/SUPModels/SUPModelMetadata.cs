﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Standups.Models.SUPModels
{

    // See: https://ryanhayes.net/data-annotations-for-entity-framework-4-entities-as-an-mvc-model/

    // Attach attribute to class in order to modify it below
    [MetadataType(typeof(SUPUserMetadata))]
    public partial class SUPUser { }

    public class SUPUserMetadata
    {
        // Name the field the same as EF named the property - "FirstName" for example.
        // Also, the type needs to match.  Basically just redeclare it.

        [Required]
        [StringLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
    }

    [MetadataType(typeof(SUPAdvisorMetadata))]
    public partial class SUPAdvisor { }

    public class SUPAdvisorMetadata
    {
        [Required]
        [StringLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
    }

    [MetadataType(typeof(SUPGroupMetadata))]
    public partial class SUPGroup { }

    public class SUPGroupMetadata
    {
        [Required]
        [StringLength(50)]
        [Display(Name = "Group Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Group Motto")]
        public string Motto { get; set; }
    }

    [MetadataType(typeof(SUPMeetingMetadata))]
    public partial class SUPMeeting { }

    public class SUPMeetingMetadata
    {
        [Display(Name = "Date Submitted")]
        public DateTime SubmissionDate { get; set; }

        [StringLength(1000)]
        [Required(ErrorMessage = "Surely you have completed something?  The Completed text is required.")]
        public string Completed { get; set; }

        [StringLength(1000)]
        [Required(ErrorMessage = "Nothing to do?  Better assign more homework!  The Planning text is required.")]
        public string Planning { get; set; }

        [StringLength(1000)]
        [Required(ErrorMessage = "Nice going -- no obstacles.  However, you can surely think of at least one.  Required.")]
        public string Obstacles { get; set; }

    }
}