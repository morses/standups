namespace Standups.Models.SUPModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SUPComment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SUPComment()
        {
            SUPCommentRatings = new HashSet<SUPCommentRating>();
        }

        public int ID { get; set; }

        public int SUPQuestionID { get; set; }

        public DateTime SubmissionDate { get; set; }

        [Required]
        [StringLength(1000)]
        public string Comment { get; set; }

        public int AdvisorRating { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SUPCommentRating> SUPCommentRatings { get; set; }

        public virtual SUPQuestion SUPQuestion { get; set; }
    }
}
