﻿using Standups.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Standups.Models.SUPModels;

namespace Standups.Models.Concrete
{
    public class EFSUPRepository : ISUPRepository
    {
        private SUPContext db = new SUPContext();

        public IEnumerable<SUPUser> SUPUsers
        {
            get { return db.SUPUsers; }
        }

        public IEnumerable<SUPAdvisor> SUPAdvisors
        {
            get { return db.SUPAdvisors; }
        }

        public IEnumerable<SUPGroup> SUPGroups
        {
            get { return db.SUPGroups; }
        }

        public IEnumerable<SUPMeeting> SUPMeetings
        {
            get { return db.SUPMeetings; }
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}