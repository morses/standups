﻿using Standups.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Standups.Models.SUPModels;

namespace Standups.Models.Concrete
{
    public class EFSUPCommentsRepository : ISUPCommentsRepository
    {
        private SUPContext db = new SUPContext();

        // Get all
        public IQueryable<SUPQuestion> Questions
        {
            get { return db.SUPQuestions; }
        }

        public IQueryable<SUPComment> Comments
        {
            get { return db.SUPComments; }
        }


        public IQueryable<SUPComment> GetCommentsFor(int questionID)
        {
            return db.SUPQuestions
                     .Where(q => q.ID == questionID)
                     .FirstOrDefault()
                     .SUPComments
                     .AsQueryable();
        }

        // CRUD for Questions
        public SUPQuestion GetQuestionById(int id)
        {
            return db.SUPQuestions.Find(id);
        }

        public SUPQuestion InsertOrUpdateQuestion(SUPQuestion question)
        {
            if(question.ID == default(int))
            {
                // Must be a new entity, so insert new
                db.SUPQuestions.Add(question);
            }
            else
            {
                // Update an existing entity
                db.Entry(question).State = System.Data.Entity.EntityState.Modified;
            }
            return question;
        }

        public void DeleteQuestion(int id)
        {
            SUPQuestion q = db.SUPQuestions.Find(id);
            db.SUPQuestions.Remove(q);
        }


        // CRUD for Comments
        public SUPComment GetCommentById(int id)
        {
            return db.SUPComments.Find(id);
        }

        public SUPComment InsertOrUpdateComment(SUPComment comment)
        {
            if (comment.ID == default(int))
            {
                // Must be a new entity, so insert new
                db.SUPComments.Add(comment);
            }
            else
            {
                // Update an existing entity
                db.Entry(comment).State = System.Data.Entity.EntityState.Modified;
            }
            return comment;
        }

        public void DeleteComment(int id)
        {
            SUPComment c = db.SUPComments.Find(id);
            db.SUPComments.Remove(c);
        }


        public bool MakeCommentRating(int userID, int commentID, int rating)
        {
            SUPUser user = db.SUPUsers.Find(userID);
            SUPComment theComment = db.SUPComments.Find(commentID);
            if (user == null || theComment == null)
            {
                return false;
            }
            // In this repo we can't use the user object obtained from the other repo
            // without throwing a Error: “An entity object cannot be referenced by multiple instances of IEntityChangeTracker"
            // changed to passing in the user ID rather than the SUPUser reference
           
            SUPCommentRating ro = user.SUPCommentRatings.FirstOrDefault(cr => cr.SUPCommentID == commentID);
            if(ro == null)
            {
                // User has not previously rated this comment
                ro = new SUPCommentRating
                {
                    SUPCommentID = commentID,
                    SUPUser = user,
                    SUPRaterUserID = user.ID,
                    RatingDate = DateTime.UtcNow,
                    RatingValue = rating
                };
                db.SUPCommentRatings.Add(ro);
            }
            else
            {
                // rating for this user and comment has been found, so just edit it
                ro.RatingValue = rating;
                db.Entry(ro).State = System.Data.Entity.EntityState.Modified;
            }
            db.SaveChanges();
            return true;
        }

        public void Save()
        {
            db.SaveChanges();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    db.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~EFSUPCommentsRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion


    }
}