﻿using Standups.Models.SUPModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Standups.Models.SUPViewModels
{
    public class MeetingsViewModel
    {
        public int classtime = 12;
        public DayOfWeek[] classdays = new DayOfWeek[] { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday };

        public IEnumerable<SUPMeeting> Meetings { get; set; }
        public SUPUser CurrentUser { get; set; }
        public int NumberOfReports
        {
            get
            {
                return Meetings.Count();
            }
        }
    }
}