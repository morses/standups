﻿using Standups.Models.SUPModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Standups.Models.SUPViewModels
{
    public class HomePageViewModel
    {
        public IEnumerable<SUPQuestion> Questions { get; set; }
    }
}