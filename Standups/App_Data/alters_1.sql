﻿
-- ########### SUPQuestions ############
-- Questions that generate comments
CREATE TABLE [dbo].[SUPQuestions]
(
	[ID] INT IDENTITY (1,1)			NOT NULL,
	[SubmissionDate] DATETIME		NOT NULL,
	[Question]		 NVARCHAR(1000)	NOT NULL,
	[Active]		 INT			NOT NULL,
	CONSTRAINT [PK_dbo.SUPQuestions] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO

-- ########### SUPComments ###########
-- Comments from students
CREATE TABLE [dbo].[SUPComments]
(
	[ID] INT IDENTITY (1,1)			NOT NULL,
--	[SUPUserID]		 INT			NOT NULL,
	[SUPQuestionID]  INT			NOT NULL,
	[SubmissionDate] DATETIME		NOT NULL,
	[Comment]		 NVARCHAR(1000)	NOT NULL,
	[AdvisorRating]  INT			NOT NULL,
	CONSTRAINT [PK_dbo.SUPComments] PRIMARY KEY CLUSTERED ([ID] ASC),
--	CONSTRAINT [FK_dbo.SUPComments_dbo.SUPUsers_ID] FOREIGN KEY ([SUPUserID]) REFERENCES [dbo].[SUPUsers] ([ID]),
	CONSTRAINT [FK_dbo.SUPComments_dbo.SUPQuestions_ID] FOREIGN KEY ([SUPQuestionID]) REFERENCES [dbo].[SUPQuestions] ([ID])
);
GO

-- ########### SUPCommentRatings ###########
-- Ratings by the students on questions
CREATE TABLE [dbo].[SUPCommentRatings]
(
	[ID] INT IDENTITY (1,1)			NOT NULL,
	[SUPCommentID]    INT			NOT NULL,
	[SUPRaterUserID]  INT			NOT NULL,
	[RatingDate]	  DATETIME		NOT NULL,
	[RatingValue]	  INT			NOT NULL,
	CONSTRAINT [PK_dbo.SUPCommentRatings] PRIMARY KEY CLUSTERED ([ID] ASC),
	CONSTRAINT [FK_dbo.SUPCommentRatings_dbo.SUPComments_ID] FOREIGN KEY ([SUPCommentID]) REFERENCES [dbo].[SUPComments] ([ID]),
	CONSTRAINT [FK_dbo.SUPCommentRatings_dbo.SUPUsers_ID] FOREIGN KEY ([SUPRaterUserID]) REFERENCES [dbo].[SUPUsers] ([ID])
);
GO

-- ########### SUPAcademicYears ###########
-- Academic Year table, so we know which students/groups to display each year
CREATE TABLE [dbo].[SUPAcademicYears]
(
	[ID]	  INT IDENTITY (1,1)	NOT NULL,
	[Year]    CHAR (9)				NOT NULL,
	CONSTRAINT [PK_dbo.SUPAcademicYears] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO

-- populate academic years table
INSERT INTO [dbo].[SUPAcademicYears] (Year) VALUES 
	('2016-2017'),
	('2017-2018'),
	('2018-2019'),
	('2019-2020'),
	('2020-2021'),
	('2021-2022'),
	('2022-2023'),
	('2023-2024'),
	('2024-2025'),
	('2025-2026');

-- Add columns to tables to use new academic year table.  ONLY FOR "MIGRATION", will update UP script
ALTER TABLE [dbo].[SUPGroups] ADD			[SUPAcademicYearID] INT NULL; 
GO
UPDATE		[dbo].[SUPGroups] SET			[SUPAcademicYearID] = 1; 
GO
ALTER TABLE [dbo].[SUPGroups] ALTER COLUMN  [SUPAcademicYearID] INT NOT NULL; 
GO
ALTER TABLE [dbo].[SUPGroups] ADD CONSTRAINT [FK_dbo.SUPGroups_dbo.SUPAcademicYears_ID] FOREIGN KEY ([SUPAcademicYearID]) REFERENCES [dbo].[SUPAcademicYears] ([ID]); 
GO



-- DROPs
-- not tested yet
ALTER TABLE [dbo].[SUPGroups] DROP CONSTRAINT [FK_dbo.SUPGroups_dbo.SUPAcademicYears_ID];

ALTER TABLE [dbo].[SUPCommentRatings] DROP CONSTRAINT [FK_dbo.SUPCommentRatings_dbo.SUPComments_ID];
ALTER TABLE [dbo].[SUPCommentRatings] DROP CONSTRAINT [FK_dbo.SUPCommentRatings_dbo.SUPUsers_ID];
ALTER TABLE [dbo].[SUPComments] DROP CONSTRAINT [FK_dbo.SUPComments_dbo.SUPUsers_ID];
ALTER TABLE [dbo].[SUPComments] DROP CONSTRAINT [FK_dbo.SUPComments_dbo.SUPQuestions_ID];

DROP TABLE [dbo].[SUPCommentRatings];
DROP TABLE [dbo].[SUPComments];
DROP TABLE [dbo].[SUPQuestions];
DROP TABLE [dbo].[SUPAcademicYears];

-- Drop foreign key on comments table to users
ALTER TABLE [dbo].[SUPComments] DROP CONSTRAINT [FK_dbo.SUPComments_dbo.SUPUsers_ID];
ALTER TABLE [dbo].[SUPComments] DROP COLUMN [SUPUserID];