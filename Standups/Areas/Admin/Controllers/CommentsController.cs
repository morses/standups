﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Standups.Models.SUPModels;
using Standups.Models.Abstract;

namespace Standups.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CommentsController : Controller
    {
        private ISUPCommentsRepository repository;

        public CommentsController(ISUPCommentsRepository repo)
        {
            repository = repo;
        }

        // GET: Admin/Comments
        public ActionResult Index()
        {
            var sUPComments = repository.Comments.Include(c => c.SUPQuestion).OrderByDescending(c => c.SubmissionDate);
            return View(sUPComments.ToList());
        }

        // GET: Admin/Comments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPComment sUPComment = repository.GetCommentById(id.Value);
            if (sUPComment == null)
            {
                return HttpNotFound();
            }
            return View(sUPComment);
        }

        // GET: Admin/Comments/Create
        public ActionResult Create()
        {
            ViewBag.SUPQuestionID = new SelectList(repository.Questions, "ID", "Question");
            return View();
        }

        // POST: Admin/Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SUPQuestionID,Comment,AdvisorRating")] SUPComment sUPComment)
        {
            if (ModelState.IsValid)
            {
                sUPComment.SubmissionDate = DateTime.UtcNow;
                repository.InsertOrUpdateComment(sUPComment);
                repository.Save();
                return RedirectToAction("Index");
            }

            ViewBag.SUPQuestionID = new SelectList(repository.Questions, "ID", "Question", sUPComment.SUPQuestionID);
            return View(sUPComment);
        }

        // GET: Admin/Comments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPComment sUPComment = repository.GetCommentById(id.Value);
            if (sUPComment == null)
            {
                return HttpNotFound();
            }
            ViewBag.SUPQuestionID = new SelectList(repository.Questions, "ID", "Question", sUPComment.SUPQuestionID);
            return View(sUPComment);
        }

        // POST: Admin/Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SUPQuestionID,SubmissionDate,Comment,AdvisorRating")] SUPComment sUPComment)
        {
            if (ModelState.IsValid)
            {
                repository.InsertOrUpdateComment(sUPComment);
                repository.Save();
                return RedirectToAction("Index");
            }
            ViewBag.SUPQuestionID = new SelectList(repository.Questions, "ID", "Question", sUPComment.SUPQuestionID);
            return View(sUPComment);
        }

        // GET: Admin/Comments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPComment sUPComment = repository.GetCommentById(id.Value);
            if (sUPComment == null)
            {
                return HttpNotFound();
            }
            return View(sUPComment);
        }

        // POST: Admin/Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            repository.DeleteComment(id);
            repository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
