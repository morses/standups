﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Standups.Models.SUPModels;
using Standups.Models.Abstract;

namespace Standups.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class QuestionsController : Controller
    {
        private ISUPCommentsRepository repository;

        public QuestionsController(ISUPCommentsRepository repo)
        {
            repository = repo;
        }

        // GET: Admin/Questions
        public ActionResult Index()
        {
            return View(repository.Questions.OrderByDescending(q => q.Active).ToList());
        }

        // GET: Admin/Questions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPQuestion sUPQuestion = repository.GetQuestionById(id.Value);
            if (sUPQuestion == null)
            {
                return HttpNotFound();
            }
            return View(sUPQuestion);
        }

        // GET: Admin/Questions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Questions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Question,Active")] SUPQuestion sUPQuestion)
        {
            if (ModelState.IsValid)
            {
                sUPQuestion.SubmissionDate = DateTime.UtcNow;
                repository.InsertOrUpdateQuestion(sUPQuestion);
                repository.Save();
                return RedirectToAction("Index");
            }

            return View(sUPQuestion);
        }

        // GET: Admin/Questions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPQuestion sUPQuestion = repository.GetQuestionById(id.Value);
            if (sUPQuestion == null)
            {
                return HttpNotFound();
            }
            return View(sUPQuestion);
        }

        // POST: Admin/Questions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SubmissionDate,Question,Active")] SUPQuestion sUPQuestion)
        {
            if (ModelState.IsValid)
            {
                repository.InsertOrUpdateQuestion(sUPQuestion);
                repository.Save();
                return RedirectToAction("Index");
            }
            return View(sUPQuestion);
        }

        // GET: Admin/Questions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPQuestion sUPQuestion = repository.GetQuestionById(id.Value);
            if (sUPQuestion == null)
            {
                return HttpNotFound();
            }
            return View(sUPQuestion);
        }

        // POST: Admin/Questions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            repository.DeleteQuestion(id);
            repository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
