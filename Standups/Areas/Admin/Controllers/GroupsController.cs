﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Standups.Models.SUPModels;

namespace Standups.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class GroupsController : Controller
    {
        private SUPContext db = new SUPContext();

        // GET: Admin/Groups
        public ActionResult Index()
        {
            var sUPGroups = db.SUPGroups.Include(s => s.SUPAcademicYear).Include(s => s.SUPAdvisor);
            return View(sUPGroups.ToList());
        }

        // GET: Admin/Groups/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPGroup sUPGroup = db.SUPGroups.Find(id);
            if (sUPGroup == null)
            {
                return HttpNotFound();
            }
            return View(sUPGroup);
        }

        // GET: Admin/Groups/Create
        public ActionResult Create()
        {
            ViewBag.SUPAcademicYearID = new SelectList(db.SUPAcademicYears, "ID", "Year");
            ViewBag.SUPAdvisorID = new SelectList(db.SUPAdvisors, "ID", "FullName");
            return View();
        }

        // POST: Admin/Groups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Motto,SUPAdvisorID,SUPAcademicYearID")] SUPGroup sUPGroup)
        {
            if (ModelState.IsValid)
            {
                db.SUPGroups.Add(sUPGroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SUPAcademicYearID = new SelectList(db.SUPAcademicYears, "ID", "Year", sUPGroup.SUPAcademicYearID);
            ViewBag.SUPAdvisorID = new SelectList(db.SUPAdvisors, "ID", "FullName", sUPGroup.SUPAdvisorID);
            return View(sUPGroup);
        }

        // GET: Admin/Groups/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPGroup sUPGroup = db.SUPGroups.Find(id);
            if (sUPGroup == null)
            {
                return HttpNotFound();
            }
            ViewBag.SUPAcademicYearID = new SelectList(db.SUPAcademicYears, "ID", "Year", sUPGroup.SUPAcademicYearID);
            ViewBag.SUPAdvisorID = new SelectList(db.SUPAdvisors, "ID", "FullName", sUPGroup.SUPAdvisorID);
            return View(sUPGroup);
        }

        // POST: Admin/Groups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Motto,SUPAdvisorID,SUPAcademicYearID")] SUPGroup sUPGroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sUPGroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SUPAcademicYearID = new SelectList(db.SUPAcademicYears, "ID", "Year", sUPGroup.SUPAcademicYearID);
            ViewBag.SUPAdvisorID = new SelectList(db.SUPAdvisors, "ID", "FullName", sUPGroup.SUPAdvisorID);
            return View(sUPGroup);
        }

        // GET: Admin/Groups/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPGroup sUPGroup = db.SUPGroups.Find(id);
            if (sUPGroup == null)
            {
                return HttpNotFound();
            }
            return View(sUPGroup);
        }

        // POST: Admin/Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SUPGroup sUPGroup = db.SUPGroups.Find(id);
            db.SUPGroups.Remove(sUPGroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
