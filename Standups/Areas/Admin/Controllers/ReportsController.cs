﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Standups.Models.SUPModels;
using Standups.Models.SUPViewModels;

namespace Standups.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ReportsController : Controller
    {
        private SUPContext db = new SUPContext();

        // GET: Admin/Reports
        public ActionResult Index()
        {
            var sUPMeetings = db.SUPMeetings.Include(s => s.SUPUser).OrderByDescending(m => m.SubmissionDate).ToList();

            MeetingsViewModel vm = new MeetingsViewModel
            {
                CurrentUser = null,
                Meetings = sUPMeetings
            };

            return View(vm);
        }

        // GET: Admin/Reports/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPMeeting sUPMeeting = db.SUPMeetings.Find(id);
            if (sUPMeeting == null)
            {
                return HttpNotFound();
            }
            return View(sUPMeeting);
        }

        // GET: Admin/Reports/Create
        public ActionResult Create()
        {
            ViewBag.SUPUserID = new SelectList(db.SUPUsers, "ID", "FirstName");
            return View();
        }

        // POST: Admin/Reports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SubmissionDate,SUPUserID,Completed,Planning,Obstacles")] SUPMeeting sUPMeeting)
        {
            if (ModelState.IsValid)
            {
                db.SUPMeetings.Add(sUPMeeting);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SUPUserID = new SelectList(db.SUPUsers, "ID", "FirstName", sUPMeeting.SUPUserID);
            return View(sUPMeeting);
        }

        // GET: Admin/Reports/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPMeeting sUPMeeting = db.SUPMeetings.Find(id);
            if (sUPMeeting == null)
            {
                return HttpNotFound();
            }
            ViewBag.SUPUserID = new SelectList(db.SUPUsers, "ID", "FirstName", sUPMeeting.SUPUserID);
            return View(sUPMeeting);
        }

        // POST: Admin/Reports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SubmissionDate,SUPUserID,Completed,Planning,Obstacles")] SUPMeeting sUPMeeting)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sUPMeeting).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SUPUserID = new SelectList(db.SUPUsers, "ID", "FirstName", sUPMeeting.SUPUserID);
            return View(sUPMeeting);
        }

        // GET: Admin/Reports/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPMeeting sUPMeeting = db.SUPMeetings.Find(id);
            if (sUPMeeting == null)
            {
                return HttpNotFound();
            }
            return View(sUPMeeting);
        }

        // POST: Admin/Reports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SUPMeeting sUPMeeting = db.SUPMeetings.Find(id);
            db.SUPMeetings.Remove(sUPMeeting);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
