﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Standups.Models.SUPModels;

namespace Standups.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdvisorsController : Controller
    {
        private SUPContext db = new SUPContext();

        // GET: Admin/Advisors
        public ActionResult Index()
        {
            return View(db.SUPAdvisors.ToList());
        }

        // GET: Admin/Advisors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPAdvisor sUPAdvisor = db.SUPAdvisors.Find(id);
            if (sUPAdvisor == null)
            {
                return HttpNotFound();
            }
            return View(sUPAdvisor);
        }

        // GET: Admin/Advisors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Advisors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FirstName,LastName")] SUPAdvisor sUPAdvisor)
        {
            if (ModelState.IsValid)
            {
                db.SUPAdvisors.Add(sUPAdvisor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sUPAdvisor);
        }

        // GET: Admin/Advisors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPAdvisor sUPAdvisor = db.SUPAdvisors.Find(id);
            if (sUPAdvisor == null)
            {
                return HttpNotFound();
            }
            return View(sUPAdvisor);
        }

        // POST: Admin/Advisors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FirstName,LastName")] SUPAdvisor sUPAdvisor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sUPAdvisor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sUPAdvisor);
        }

        // GET: Admin/Advisors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SUPAdvisor sUPAdvisor = db.SUPAdvisors.Find(id);
            if (sUPAdvisor == null)
            {
                return HttpNotFound();
            }
            return View(sUPAdvisor);
        }

        // POST: Admin/Advisors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SUPAdvisor sUPAdvisor = db.SUPAdvisors.Find(id);
            db.SUPAdvisors.Remove(sUPAdvisor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
