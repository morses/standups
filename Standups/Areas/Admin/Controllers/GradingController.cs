﻿using Standups.Models.SUPModels;
using Standups.Models.SUPViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Standups.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class GradingController : Controller
    {

        private SUPContext db = new SUPContext();

        /// <summary>
        /// Create landing page for reports.  Most data will be sent via
        /// JSON asynchronously so this ActionResult doesn't have to contain
        /// much.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// AJAX call to get all users, groups and advisors.
        /// </summary>
        /// <returns>JSON object containing Users, Groups and Advisors, each
        /// an array of current rows in each respective table.</returns>
        public JsonResult GetUGA()
        {
            // all have the form of {Name="", Id=""}
            // all users
            var users = db.SUPUsers
                .OrderBy(u => u.LastName)
                .Select(u => new { Name = u.FirstName + " " + u.LastName, Id = u.ID });
            // all groups
            var groups = db.SUPGroups
                .OrderBy(g => g.Name)
                .Select(g => new { Name = g.Name, Id = g.ID });
            // all advisors
            var advisors = db.SUPAdvisors
                .OrderBy(a => a.LastName)
                .Select(a => new { Name = a.FirstName + " " + a.LastName, Id = a.ID });
            var data = new { Users = users, Groups = groups, Advisors = advisors };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        private Object BuildJsonObject(MeetingsViewModel vm)
        {
            var data = vm.Meetings
                    //.OrderByDescending(s => s.SubmissionDate)
                    .Select(s => new {
                        ID = s.ID,
                        Date = s.TimeStampInPacificTimeZone.ToString(),
                        MeetsCriteria = s.TimeStampMeetsCriteria(vm.classtime, vm.classdays),
                        DOW = s.DayOfWeekString,
                        Name = s.SUPUser.FullName,
                        Group = s.SUPUser.SUPGroup.Name,
                        Advisor = s.SUPUser.SUPGroup.SUPAdvisor.LastName,
                        Completed = s.Completed,
                        Planning = s.Planning,
                        Obstacles = s.Obstacles
                    });
            return data;
        }

        /// <summary>
        /// AJAX call to get Stand Up Reports for a given User, Group or Advisor.
        /// </summary>
        /// <param name="user">UserID of a user to find SUPs for. If null, goes on to groups</param>
        /// <param name="group">GroupID of a group to find SUPs for. If null, goes on to advisors</param>
        /// <param name="advisor">AdvisorID of an advisor to find SUPs for.</param>
        /// <returns></returns>
        public JsonResult GetSUPs(int? user, int? group, int? advisor)
        {
            MeetingsViewModel vm = new MeetingsViewModel();
            vm.CurrentUser = null;

            if (user != null)
            {
                vm.Meetings = db.SUPMeetings.Where(s => s.SUPUserID == user)
                                            .OrderByDescending(s => s.SubmissionDate)
                                            .ToList();
                var data = BuildJsonObject(vm);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else if (group != null)
            {
                /*var data = db.SUPMeetings
                    .Where(s => s.SUPUser.SUPGroupID == group)
                    .OrderBy(s => s.SubmissionDate)
                    .Select(s => new { ID = s.ID, Date = s.SubmissionDate, User = s.SUPUser, Group = s.SUPUser.SUPGroup, Advisor = s.SUPUser.SUPGroup.SUPAdvisor })
                    .AsEnumerable()
                    .Select(s => new { ID = s.ID, Date = s.Date.ToString(), Name = s.User.FirstName + " " + s.User.LastName, Group = s.Group.Name, Advisor = s.Advisor.LastName });
*/
                vm.Meetings = db.SUPMeetings.Where(s => s.SUPUser.SUPGroupID == group)
                                            .OrderByDescending(s => s.SubmissionDate)
                                            .ThenBy(s => s.SUPUser.LastName)
                                            .ToList();
                var data = BuildJsonObject(vm);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else if (advisor != null)
            {
                /* var data = db.SUPMeetings
                     .Where(s => s.SUPUser.SUPGroup.SUPAdvisorID == advisor)
                     .OrderBy(s => s.SubmissionDate)
                     .Select(s => new { ID = s.ID, Date = s.SubmissionDate, User = s.SUPUser, Group = s.SUPUser.SUPGroup, Advisor = s.SUPUser.SUPGroup.SUPAdvisor })
                     .AsEnumerable()
                     .Select(s => new { ID = s.ID, Date = s.Date.ToString(), Name = s.User.FirstName + " " + s.User.LastName, Group = s.Group.Name, Advisor = s.Advisor.LastName });
                     */
                vm.Meetings = db.SUPMeetings.Where(s => s.SUPUser.SUPGroup.SUPAdvisorID == advisor)
                                                            .OrderByDescending(s => s.SubmissionDate)
                                                            .ThenBy(s => s.SUPUser.SUPGroupID)
                                                            .ToList();
                var data = BuildJsonObject(vm);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            return Json(new List<object>(), JsonRequestBehavior.AllowGet);
        }
    }
}