﻿using Microsoft.AspNet.Identity.Owin;
using Standups.Models.SUPModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Standups.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private ApplicationUserManager _userManager;
        private SUPContext db = new SUPContext();

        public ActionResult Index()
        {
            return View();
        }

        // GET: List view of all SUPUsers
        public ActionResult Users()
        {
            return View(db.SUPUsers);
        }

        public ActionResult Reports()
        {
            return View(db.SUPMeetings);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}