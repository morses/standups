﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using Standups.Models;
using System.Diagnostics;

[assembly: OwinStartupAttribute(typeof(Standups.Startup))]
namespace Standups
{
    public partial class Startup
    {
        public const string ADMIN_ROLE_NAME = "Admin";

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesandUsers();
        }

        // See: https://code.msdn.microsoft.com/ASPNET-MVC-5-Security-And-44cbdb97
        // In this method we will create default User roles and Admin user for login   
        private void CreateRolesandUsers()
        {
            // The context that Identity created
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            // Create admin role and seed with the admin user/
            // Assumes neither already exists
            if (!roleManager.RoleExists(ADMIN_ROLE_NAME))
            {  
                // Create role
                var role = new IdentityRole(ADMIN_ROLE_NAME);       // role name is "Admin"
                IdentityResult res = roleManager.Create(role);

                // Create user with this role
                string userPWD = System.Web.Configuration.WebConfigurationManager.AppSettings["AdminPassword"];
                string userEmail = System.Web.Configuration.WebConfigurationManager.AppSettings["AdminEmail"];
                var user = new ApplicationUser { UserName = userEmail, Email = userEmail };
                // Username and email must be the same unless you want to make changes to the login code, which assumes they are the same
                // It will appear to work but once you clear your cache (to delete the cookie) or use another browser it won't work
                
                res = UserManager.Create(user, userPWD);

                if (res.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, ADMIN_ROLE_NAME);
                }
            }

            // Do we need another role?  i.e. "User"

            // creating Creating Employee role   
            /*
            if (!roleManager.RoleExists("Employee"))
            {
                var role = new IdentityRole();
                role.Name = "Employee";
                roleManager.Create(role);
            }
            */
        }
    }
}
